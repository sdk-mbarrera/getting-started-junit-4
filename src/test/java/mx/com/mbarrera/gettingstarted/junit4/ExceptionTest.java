package mx.com.mbarrera.gettingstarted.junit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class ExceptionTest {

    private final List<String> listToVerify = new ArrayList<>();
    @Rule
    public ExpectedException ruleForVerifyException = ExpectedException.none();

    @Test(expected = IndexOutOfBoundsException.class)
    public void verifyExceptionWithTheTestAnnotation() {
        listToVerify.get(0);
    }

    @Test
    public void verifyExceptionIntoCatchBlock() {
        try {
            listToVerify.get(0);
            fail("Expected an IndexOutOfBoundsException to be thrown");
        } catch (IndexOutOfBoundsException anIndexOutOfBoundsException) {
            assertThat(anIndexOutOfBoundsException.getMessage(), is("Index: 0, Size: 0"));
        }
    }

    @Test
    public void verifyExceptionWithRule() {
        ruleForVerifyException.expect(IndexOutOfBoundsException.class);
        ruleForVerifyException.expectMessage("Index: 0, Size: 0");

        listToVerify.get(0);
    }
}
