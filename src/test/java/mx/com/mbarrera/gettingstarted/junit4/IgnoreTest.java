package mx.com.mbarrera.gettingstarted.junit4;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IgnoreTest {

    @Ignore("Test ignored")
    @Test
    public void testIgnored() {
        assertThat(1, is(1));
    }

    @Test
    public void testNotIgnored() {
        assertThat(1, is(1));
    }
}
