package mx.com.mbarrera.gettingstarted.junit4;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MethodOrderTest {

    @Test
    public void testMethodA() {
        System.out.println("first");
    }

    @Test
    public void testMethodB() {
        System.out.println("second");
    }

    @Test
    public void testMethodC() {
        System.out.println("third");
    }
}
